// Tabs
(function ($) {
    $(function () {
        $("ul.tabs__caption").on("click", "li:not(.active)", function () {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.tabs")
                .find("div.tabs__content")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
    });
})(jQuery);


// Gallery Filter
$(document).ready(function () {
    $('#filterOptions li a').click(function () {
        let ourClass = $(this).attr('class');
        let categoryGallery = $('.categoryGallery');

        $('#filterOptions li').removeClass('active');

        $(this).parent().addClass('active');
        if (ourClass === 'all') {

            categoryGallery.find('div.item').show();
        } else {
            categoryGallery.find('div.item').hide();

            categoryGallery.find('div.' + ourClass).show();
        }
        return false;
    });
});

// Button Load more

function createNewImg() {
    $('#loadMoreButton').remove();
    $('.preloader').css('display', 'block');
    setTimeout(getMoreImgs, 2000);

    function getMoreImgs() {
        $('.itemsNonShow').removeClass('hidden');
        $('.preloader').css('display', 'none');
    }
}

$('#loadMoreButton').on('click', createNewImg);


$('.slickBloc').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    asNavFor: '.slickSlider'
    // adaptiveHeight: true
});

$('.slickSlider').slick({
    dots: false,
    centerMode: true,
    arrows: true,
    prevArrow: '.chevronLeft',
    nextArrow: '.chevronRight',
    // infinite: true,
    speed: 300,
    slidesToScroll: 1,
    // slidesToShow: 4,
    // adaptiveHeight: true,
    asNavFor: '.slickBloc',
    focusOnSelect: true,
    variableHeight: true,
    variableWidth: true
});


//
// // Button Load more
// (function ($) {
//     'use strict';
//
//     $.fn.loadMoreResults = function (options) {
//
//         var defaults = {
//             tag: {
//                 name: 'div',
//                 'class': 'item'
//             },
//             displayedItems: 5,
//             showItems: 5,
//             button: {
//                 'class': 'btn-load-more',
//                 text: 'Load More'
//             }
//         };
//
//         var opts = $.extend(true, {}, defaults, options);
//
//         var alphaNumRE = /^[A-Za-z][-_A-Za-z0-9]+$/;
//         var numRE = /^[0-9]+$/;
//
//         $.each(opts, function validateOptions(key, val) {
//             if (key === 'tag') {
//                 formatCheck(key, val, 'name', 'string');
//                 formatCheck(key, val, 'class', 'string');
//             }
//             if (key === 'displayedItems') {
//                 formatCheck(key, val, null, 'number');
//             }
//             if (key === 'showItems') {
//                 formatCheck(key, val, null, 'number');
//             }
//             if (key === 'button') {
//                 formatCheck(key, val, 'class', 'string');
//             }
//         });
//
//         function formatCheck(key, val, prop, typ) {
//             if (prop !== null && typeof prop !== 'object') {
//                 if (typeof val[prop] !== typ || String(val[prop]).match(typ == 'string' ? alphaNumRE : numRE) === null) {
//                     opts[key][prop] = defaults[key][prop];
//                 }
//             } else {
//                 if (typeof val !== typ || String(val).match(typ == 'string' ? alphaNumRE : numRE) === null) {
//                     opts[key] = defaults[key];
//                 }
//             }
//         };
//
//         return this.each(function (index, element) {
//             var $list = $(element),
//                 lc = $list.find(' > ' + opts.tag.name + '.' + opts.tag.class).length,
//                 dc = parseInt(opts.displayedItems),
//                 sc = parseInt(opts.showItems);
//
//             $list.find(' > ' + opts.tag.name + '.' + opts.tag.class + ':lt(' + dc + ')').css("display", "inline-block");
//             $list.find(' > ' + opts.tag.name + '.' + opts.tag.class + ':gt(' + (dc - 1) + ')').css("display", "none");
//
//             $list.parent().append('<button class="btn-view ' + opts.button.class + '">' + opts.button.text + '</button>');
//             $list.parent().on("click", ".btn-view", function (e) {
//                 e.preventDefault();
//                 dc = (dc + sc <= lc) ? dc + sc : lc;
//
//                 $list.find(' > ' + opts.tag.name + '.' + opts.tag.class + ':lt(' + dc + ')').fadeIn();
//                 if (dc == lc) {
//                     $(this).hide();
//                 }
//             });
//         });
//
//     };
// })(jQuery);
